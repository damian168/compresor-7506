#!/bin/bash

echo "Pruebas con distintos modelos de calculo de probabilidades para PPMC de orden $1"
for f in icon.xpm about.html BIBLIA.txt; do
    echo "*********************************************************************"
    echo "Pruebas con el archivo $f"
    echo "-----"
    echo "tamaño del archivo: " 
    du -h $f
    echo "-----"
    echo "1) Compresion clasica: "
    ./Compresor -c $1 $f
    echo "-----"
    echo "2) Compresion con evejecimiento hiperbolico: "
    ./Compresor -ceh $1 $f
    echo "-----"
    echo "3) Compresion con modelo cuadratico: "
    ./Compresor -cmc $1 $f
    echo "-----"
    echo "4.1.1) Compresion con modelo geometrico sumando 1 y K=0.1425: "
    ./Compresor -ceg 1 0.1425 $1 $f
    echo "4.1.2) Compresion con modelo geometrico sumando 1 y K=0.1: "
    ./Compresor -ceg 1 0.1 $1 $f
    echo "4.1.3) Compresion con modelo geometrico sumando 1 y K=0.05: "
    ./Compresor -ceg 1 0.05 $1 $f
    echo "4.1.4) Compresion con modelo geometrico sumando 1 y K=0.025: "
    ./Compresor -ceg 1 0.025 $1 $f
    echo "4.1.5) Compresion con modelo geometrico sumando 1 y K=0: "
    ./Compresor -ceg 1 0 $1 $f
    echo "-----"
    echo "4.2.1) Compresion con modelo geometrico sumando 2 y K=0.1425: "
    ./Compresor -ceg 2 0.1425 $1 $f
    echo "4.2.2) Compresion con modelo geometrico sumando 2 y K=0.1: "
    ./Compresor -ceg 2 0.1 $1 $f
    echo "4.2.3) Compresion con modelo geometrico sumando 2 y K=0.05: "
    ./Compresor -ceg 2 0.05 $1 $f
    echo "4.2.4) Compresion con modelo geometrico sumando 2 y K=0.025: "
    ./Compresor -ceg 2 0.025 $1 $f
    echo "4.2.5) Compresion con modelo geometrico sumando 2 y K=0: "
    ./Compresor -ceg 2 0 $1 $f
    echo "-----"
    echo "4.3.1) Compresion con modelo geometrico sumando 3 y K=0.1425: "
    ./Compresor -ceg 3 0.1425 $1 $f
    echo "4.3.2) Compresion con modelo geometrico sumando 3 y K=0.1: "
    ./Compresor -ceg 3 0.1 $1 $f
    echo "4.3.3) Compresion con modelo geometrico sumando 3 y K=0.05: "
    ./Compresor -ceg 3 0.05 $1 $f
    echo "4.3.4) Compresion con modelo geometrico sumando 3 y K=0.025: "
    ./Compresor -ceg 3 0.025 $1 $f
    echo "4.3.5) Compresion con modelo geometrico sumando 3 y K=0: "
    ./Compresor -ceg 3 0 $1 $f
    echo "-----"
    echo "4.4.1) Compresion con modelo geometrico sumando 4 y K=0.1425: "
    ./Compresor -ceg 4 0.1425 $1 $f
    echo "4.4.2) Compresion con modelo geometrico sumando 4 y K=0.1: "
    ./Compresor -ceg 4 0.1 $1 $f
    echo "4.4.3) Compresion con modelo geometrico sumando 4 y K=0.05: "
    ./Compresor -ceg 4 0.05 $1 $f
    echo "4.4.4) Compresion con modelo geometrico sumando 4 y K=0.025: "
    ./Compresor -ceg 4 0.025 $1 $f
    echo "4.4.5) Compresion con modelo geometrico sumando 4 y K=0: "
    ./Compresor -ceg 4 0 $1 $f
    echo "-----"
    echo "5) Compresion con ESC inicializado a 1 e incremento 3:"
    ./Compresor -cie $1 $f
    echo "*********************************************************************"
done

