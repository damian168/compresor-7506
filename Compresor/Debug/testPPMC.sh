#!/bin/bash

echo "Comparación de compresión entre el modelo clasico y el mejorado para PPMC de orden $1"
for f in icon.xpm about.html BIBLIA.txt; do
    echo "*********************************************************************"
    echo "1) Compresion clasica: "
    ./Compresor -c -mostrar-tamano -orden $1 -modelo n $f
    echo "-----"
    echo "2) Compresion con ESC inicializado a 1 e incremento 3:"
    ./Compresor -c -mostrar-tamano -orden $1 -modelo ie $f
    echo "*********************************************************************"
done

