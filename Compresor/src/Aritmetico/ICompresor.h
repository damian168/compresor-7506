/*
 * ICompresor.h
 *
 *  Created on: Apr 14, 2012
 *      Author: dfranetovich
 */

#ifndef ICOMPRESOR_H_
#define ICOMPRESOR_H_
#include "../PPMC/Modelo.h"

class ICompresor
{
public:

    virtual void comprimir(const Simbolo simbolo, const FrecuenciasSimbolos &frecuencias,
            const FrecuenciasSimbolos &excluir, std::ostream &salida)=0;

    virtual void comprimirEquiprovable(const Simbolo simbolo,
            const FrecuenciasSimbolos &frecuencias, std::ostream &salida)=0;

    virtual Simbolo descomprimir(const FrecuenciasSimbolos &frecuencias,
            const FrecuenciasSimbolos &excluir, std::istream &entrada)=0;

    virtual Simbolo descomprimirEquiprovable(const FrecuenciasSimbolos &frecuencias,
            std::istream &entrada) = 0;

    virtual ~ICompresor()
    {
    }
    ;
};

#endif /* ICOMPRESOR_H_ */
