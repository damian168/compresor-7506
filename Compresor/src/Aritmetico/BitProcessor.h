/*
 * BitProcessor.h
 *
 *  Created on: Apr 21, 2012
 *      Author: dfranetovich
 */

#ifndef BITPROCESSOR_H_
#define BITPROCESSOR_H_
#define BUFFER_SIZE 1
#include <fstream>
#include "inttypes.h"
#include <iostream>

using namespace std;

class BitProcessor
{
    string buffer;
    int bufferIndex;
    char byte;
    char bitIndex;

public:
    BitProcessor();
    virtual ~BitProcessor();

    bool eof(istream& entrada);
    void flush(ostream& salida);
    void escribir_bit(uint32_t value, ostream& salida);
    uint32_t leer_bit(istream& entrada);
};

#endif /* BITPROCESSOR_H_ */
