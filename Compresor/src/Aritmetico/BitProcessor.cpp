/*
 * BitProcessor.cpp
 *
 *  Created on: Apr 21, 2012
 *      Author: dfranetovich
 */

#include "BitProcessor.h"

BitProcessor::BitProcessor()
{
    buffer = "";
    bufferIndex = 0;
    byte = 0x00;
    bitIndex = 0;
}

BitProcessor::~BitProcessor()
{
    // TODO Auto-generated destructor stub
}

void BitProcessor::escribir_bit(uint32_t value, ostream& salida)
{
//    cout << (value == 0 ? "0" : "1");
    //	escribir bit
    byte <<= 1;
    byte += (value == 0 ? 0x0000 : 0x0001);
    bitIndex++;
    if ((bitIndex % 8) == 0)
    {
        buffer += byte;
        bufferIndex++;
        if ((bufferIndex % BUFFER_SIZE) == 0)
        {
            salida.write(buffer.c_str(), buffer.size());
            buffer = "";
            bufferIndex = 0;
        }
        bitIndex = 0;
    }
}

bool BitProcessor::eof(istream& entrada)
{
    bool eof = entrada.eof() && (buffer.size() == 0) && (bitIndex % 8 == 0);
    if (eof)
    {
        return true;
    }
    return eof;
}

void BitProcessor::flush(ostream& salida)
{
    while ((bitIndex % 8) != 0)
    {
        escribir_bit(0x0000, salida);
    }
    unsigned long int size = buffer.size();
    if (size != 0)
    {
        salida.write(buffer.c_str(), size);
    }
}

uint32_t BitProcessor::leer_bit(istream& entrada)
{
    unsigned short int bit = 0x0000;

    if ((bitIndex % 8) == 0)
    {
        if ((bufferIndex % BUFFER_SIZE) == 0)
        {
            char* readValue = new char[BUFFER_SIZE];
            for (int i = 0; i < BUFFER_SIZE; i++)
            {
                readValue[i] = 0;
            }
            bool good = entrada.read(readValue, BUFFER_SIZE).good();
            if (good)
            {
                buffer = readValue;
            }
            else
            {
                buffer = "";
                int i = 0;
                while (readValue[i] != 0)
                {
                    buffer += readValue[i];
                }
            }
            bufferIndex = 0;
            delete[] readValue;
        }
        byte = buffer[bufferIndex];
        bufferIndex++;
        bitIndex = 0;
    }

    bit = byte & 0x80;
    byte <<= 1;
    bitIndex++;
//    cout << (bit == 0x0000 ? "0" : "1");
    return bit == 0 ? 0x00000000 : 0x00000001;
}

int mainn(int argc, char *argv[])
{
    BitProcessor bp1;
    BitProcessor bp2;
    ifstream entrada;
    ofstream salida;
    entrada.open("archivo");
    salida.open("archivo.sal");
    uint32_t bit = bp1.leer_bit(entrada);
    while (!bp1.eof(entrada))
    {
        bp2.escribir_bit(bit, salida);
        bit = bp1.leer_bit(entrada);
    }
    bp2.flush(salida);
    salida.close();
    entrada.close();
    return 0;
}
