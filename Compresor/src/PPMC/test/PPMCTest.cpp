#include <iostream>
#include <fstream>
#include <string>
#include "../PPMC.h"
#include "PPMCTest.h"

#define NUMERO_GRUPO "XX"

using namespace std;

bool archivosIguales(const string &nombreArchivoX,const string &nombreArchivoY)
{
	ifstream x,y;
	x.open(nombreArchivoX.c_str(),ios_base::in | ios_base::binary);
	y.open(nombreArchivoY.c_str(),ios_base::in | ios_base::binary);
	bool archivosIguales=true;
	bool seguir=true;
	char cx,cy;

	x.read(&cx,sizeof(char));
	y.read(&cy,sizeof(char));

	seguir=((not x.eof()) && (not y.eof()));

	while (seguir)
	{
		archivosIguales=(cx==cy);
		x.read(&cx,sizeof(char));
		y.read(&cy,sizeof(char));
		seguir=((not x.eof()) && (not y.eof()) && archivosIguales);
	}

	if (archivosIguales)
		archivosIguales=(x.eof()&&y.eof());

	x.close();
	y.close();

	return archivosIguales;
}

void comprimirDescomprimirComparar(string contenido)
{
	PPMC *compresor=PPMC::getNewInstance(3);

	ofstream archivoInicial;

	archivoInicial.open("archivoInicial.tmp",ios_base::trunc | ios_base::binary);
	archivoInicial << contenido;
	archivoInicial.close();

	ifstream entradaAComprimir;
	entradaAComprimir.open("archivoInicial.tmp",ios_base::in | ios_base::binary);
	ofstream salidaComprimida;
	salidaComprimida.open("archivoComprimido.tmp",
		ios_base::binary | ios_base::trunc);
	compresor->comprimir(entradaAComprimir,salidaComprimida);
	entradaAComprimir.close();
	salidaComprimida.close();

	PPMC *descompresor=PPMC::getNewInstance(3);
	ifstream entradaADescomprimir;
	entradaADescomprimir.open("archivoComprimido.tmp",
		ios_base::binary | ios_base::in);
	ofstream salidaDescomprimida;
	salidaDescomprimida.open("archivoDescomprimido.tmp",
		ios_base::trunc | ios_base::binary);
	descompresor->descomprimir(entradaADescomprimir,salidaDescomprimida);
	entradaADescomprimir.close();
	salidaDescomprimida.close();

	cout << "El contenido " << contenido << " se ha comprimido y decomprimido, " <<
		"el contenido resultante es igual al inicial: " <<
		archivosIguales("archivoInicial.tmp","archivoDescomprimido.tmp")
		<< endl;

	PPMC::deleteInstance();
}

int ppmcTest()
{
	cout << endl;
	cout << "=======================" << endl;
	cout << "P R U E B A S   P P M C" << endl;
	cout << "=======================" << endl;
	cout << endl;

	comprimirDescomprimirComparar("");
	comprimirDescomprimirComparar("ABCD");
	comprimirDescomprimirComparar("ABCDABCD");
	comprimirDescomprimirComparar("ABCDABCDABCDABCD");
	comprimirDescomprimirComparar("Hola esto es una prueba!!!\nHola esto es una prueba!!!\nHola esto es una prueba!!!\nHola esto es una prueba!!!\nHola esto es una prueba!!!\n");

	return 0;
}
